Thanks for helping us with this project!

Please, fork this repo, clone it, make your changes locally and submit your Merge Request. We appreciate small Merge Request so we can validate the changes. Should you have any question, please reach me on google chat or mail vestival@redhat.com